imod.wq  -  Create Water Quality model
--------------------------------------

.. automodule:: imod.wq
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:
