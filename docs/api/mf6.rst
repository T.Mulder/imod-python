imod.mf6  -  Create Modflow 6 model
-----------------------------------

.. automodule:: imod.mf6
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:
