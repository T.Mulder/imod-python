imod.prepare  -  Prepare model input
------------------------------------

.. automodule:: imod.prepare
    :members:
    :imported-members:
    :undoc-members:
    :show-inheritance:
